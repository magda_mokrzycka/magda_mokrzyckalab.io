# Opis strony

Strona została stworzona podczas trwania kursu testerskiego, jako projekt zaliczeniowy tworzenia stron w języku HTML (z wykorzystaniem framework'a Bootstrap).

# Demo 

Link do strony: [https://magda_mokrzycka.gitlab.io](https://magda_mokrzycka.gitlab.io)
